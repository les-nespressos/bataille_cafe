﻿using System;
using System.Collections.Generic;
using System.Text;

namespace nespresso
{
    public class Parcelle
    {
        public char nom { get; set; }
        public List<Case> Cases { get; set; }
        public int Casesjoueesclient { get; set; }

        public int Casesjoueesserveur { get; set; }

        public Parcelle(char p_nom)
        {
            nom = p_nom;
            Cases = new List<Case> ();
            Casesjoueesclient = 0;
            Casesjoueesserveur = 0;
        }


    }
}
