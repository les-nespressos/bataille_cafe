﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;

namespace nespresso
{
    class Communication
    {

        // Procédure qui nous permet de jouer à chaque tour contre le serveur c'est la partie principale du code, 
        //on utilise donc la carte avec tous les éléments qu'elle possède (ilot, parcelles, cases, ...)
        public static void TalkToServ(Socket mySocket, Carte carte)
        {
            bool partie = true;
            bool premier_tour = true;
            string coup_serveur = "";
            do
            {
                char lettre;
                string coup_client = "";
                string reponse_serveur = "";
                string reponsemessage_serveur = "";

                //Envoie coordonnées
                if (premier_tour == true)
                {
                    coup_client = Combat.algo_premiertour(carte);
                    premier_tour = false;
                }
                else
                {
                    coup_client = Combat.algo_combat(carte, coup_serveur);
                }

                byte[] coordonnees = Encoding.ASCII.GetBytes(coup_client);
                int byteSent = mySocket.Send(coordonnees);

                //data buffer
                byte[] messageRecu = new byte[4];

                //Recevoir la réponse
                try
                {
                    int Reponse = mySocket.Receive(messageRecu);
                    reponse_serveur = Encoding.ASCII.GetString(messageRecu, 0, Reponse);
                    Console.WriteLine("Message du serveur -> " + reponse_serveur);
                }
                catch (SocketException e1)
                {
                    Console.WriteLine("SocketException : {0}", e1.ToString());
                }

                if (reponse_serveur == "VALI")
                {
                    lettre = carte.ilot.liste_cases[int.Parse(coup_client.Substring(2, 1)), int.Parse(coup_client.Substring(3, 1))].lettre_parcelle;
                    carte.ilot.Parcelles[(int)lettre - 97].Casesjoueesclient += 1;
                    carte.ilot.liste_cases[int.Parse(coup_client.Substring(2, 1)), int.Parse(coup_client.Substring(3, 1))].jouee_client = true;
                    carte.ilot.liste_cases[int.Parse(coup_client.Substring(2, 1)), int.Parse(coup_client.Substring(3, 1))].jouee = true;
                }

                //tour du serveur
                byte[] placement = new byte[4];

                try
                {
                    int tourServ = mySocket.Receive(placement);
                    coup_serveur = Encoding.ASCII.GetString(placement, 0, tourServ);
                    Console.WriteLine("L'IA place sa graine aux coordonnées -> " + coup_serveur);
                }
                catch (SocketException e1)
                {
                    Console.WriteLine("SocketException : {0}", e1.ToString());
                }

                if (coup_serveur != "FINI")
                {
                    lettre = carte.ilot.liste_cases[int.Parse(coup_serveur.Substring(2, 1)), int.Parse(coup_serveur.Substring(3, 1))].lettre_parcelle;
                    carte.ilot.Parcelles[(int)lettre - 97].Casesjoueesserveur += 1;
                    carte.ilot.liste_cases[int.Parse(coup_serveur.Substring(2, 1)), int.Parse(coup_serveur.Substring(3, 1))].jouee_serveur = true;
                    carte.ilot.liste_cases[int.Parse(coup_serveur.Substring(2, 1)), int.Parse(coup_serveur.Substring(3, 1))].jouee = true;


                    //Recevoir la réponse
                    byte[] messageserveur = new byte[4];
                    
                    try
                    {
                        int ReponseAdv = mySocket.Receive(messageserveur);
                        reponsemessage_serveur = Encoding.ASCII.GetString(messageserveur, 0, ReponseAdv);
                        Console.WriteLine("Message du serveur -> " + reponsemessage_serveur);
                    }
                    catch (SocketException e1)
                    {
                        Console.WriteLine("SocketException : {0}", e1.ToString());
                    }
                }
                carte.ilot.afficher_ilot();

                if (reponsemessage_serveur.Equals("FINI") || coup_serveur.Equals("FINI"))
                {
                    byte[] messagescore = new byte[8];
                    Console.WriteLine("FIN");
                    int Reponsescore = mySocket.Receive(messagescore);
                    string score = Encoding.ASCII.GetString(messagescore, 0, Reponsescore);
                    Console.WriteLine("Score final : " + score);
                    partie = false;
                }

            } while (partie == true);
        }
    }
}
