﻿using System;
using System.Collections.Generic;
using System.Text;

namespace nespresso
{
    public class Case
    {
        public bool nord { get; set; }
        public bool ouest { get; set; }
        public bool sud { get; set; }
        public bool est { get; set; }
        public string etat { get; set; }
        public bool remplie { get; set; }
        public int nombre { get; set; }
        public int abcisse { get; set; }
        public int ordonnee { get; set; }
        public char lettre_parcelle { get; set; }
        public bool jouee { get; set; }
        public bool jouee_client { get; set; }
        public bool jouee_serveur { get; set; }
        public Case()
        {
            nord = false;
            ouest = false;
            sud = false;
            est = false;
            remplie = false;
            nombre = 0;
            jouee = false;
            jouee_client = false;
            jouee_serveur = false;
        }

        //Fonction de décodage de chaque case par rapport à la trame
        public void decodeUneCase()
        {
            int nombre_actuel = nombre;
            if (nombre / 64 == 1)
            {
                etat = "mer";
                nombre = nombre - 64;
            }
            if (nombre / 32 == 1)
            {
                etat = "foret";
                nombre = nombre - 32;
            }
            if (nombre / 8 == 1)
            {
                est = true;
                nombre = nombre - 8;
            }

            if (nombre / 4 == 1)
            {
                sud = true;
                nombre = nombre - 4;
            }

            if (nombre / 2 == 1)
            {
                ouest = true;
                nombre = nombre - 2;
            }

            if (nombre == 1)
            {
                nord = true;
            }
        }

        //Cette fonction permet d'afficher la carte, on regarde de quel type est la case et si elle est jouée pour chacune d'entre elles
        public string Parcours_case(string p_chaine)
        {
            if (etat == "mer")
                p_chaine += "M ";
            else if (etat == "foret")
                p_chaine += "F ";
            else if (jouee_client == true)
                p_chaine += "A ";
            else if(jouee_serveur == true)
                p_chaine += "B ";
            else
                p_chaine += lettre_parcelle + " ";

            if (est == true)
                p_chaine += "| ";
            else
                p_chaine += "  ";

            return p_chaine;
        }
    }
}
