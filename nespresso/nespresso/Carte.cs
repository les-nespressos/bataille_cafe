﻿using System;
using System.Collections.Generic;
using System.Text;

namespace nespresso
{
    public class Carte
    {
        public string trame { get; set; }
        public int[,] map { get; set; }

        public Ilot ilot { get; set; }

        public Carte(string p_trame)
        {
            trame = p_trame;
            map = decoupage_trame(10);
            ilot = new Ilot();
        }
        
        public void creation_ilot()
        {
            for (int ligne = 0; ligne < 10; ligne++)
            {
                for (int colonne = 0; colonne < 10; colonne++)
                {
                    ilot.liste_cases[ligne, colonne] = new Case();
                    ilot.liste_cases[ligne, colonne].nombre = map[ligne, colonne];
                    ilot.liste_cases[ligne, colonne].abcisse = ligne;
                    ilot.liste_cases[ligne, colonne].ordonnee = colonne;
                }
            }
        }

        public int[,] decoupage_trame(int p_taille_carte)
        {
            int[,] carte = new int[p_taille_carte, p_taille_carte];

            string[] new_trame = new string[10];
            new_trame = trame.Split('|');

            for (int i = 0; i < p_taille_carte; i++)
            {
                string[] ligne_trame = new_trame[i].Split(':');
                for (int j = 0; j < p_taille_carte; j++)
                {
                    carte[i, j] = int.Parse(ligne_trame[j]);
                }
            }
            return carte;
        }

        public void initialisation()
        {
            creation_ilot();
            ilot.traduction_ilot();
            ilot.Valeurs_parcelles();
            ilot.afficher_ilot();
        }
    }
}
