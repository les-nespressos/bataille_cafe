﻿using System;
using System.Net.Sockets;
using System.Text;

namespace nespresso
{
    public class main
    {
        static void Main(string[] args)
        {
            Socket mySocket = Connexion.Create_Socket();
            Connexion.Connect_Socket(mySocket);
            string trame = Connexion.Recup_Trame(mySocket);
            if (trame != null)
            {
                Carte carte = new Carte(trame);
                carte.initialisation();
                Communication.TalkToServ(mySocket, carte);
                Connexion.destroy_Socket(mySocket);
            }
            else
                Console.WriteLine("\n\nServeur non connecté");

        }
    }
}
