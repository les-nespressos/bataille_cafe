﻿using System;
using System.Collections.Generic;
using System.Text;

namespace nespresso
{
    public class Ilot
    {
        public Case[,] liste_cases { get; set; }
        public List<Parcelle> Parcelles { get; set; }

        public int nb_parcelles { get; set; }

        public Ilot()
        {
            liste_cases = new Case[10, 10];
            Parcelles = new List<Parcelle>();
            nb_parcelles = 0;
        }

        public void creation_ilot(int[,] carte)
        {
            for (int ligne = 0; ligne < 10; ligne++)
            {
                for (int colonne = 0; colonne < 10; colonne++)
                {
                    liste_cases[ligne, colonne] = new Case();
                    liste_cases[ligne, colonne].nombre = carte[ligne, colonne];
                }
            }
        }

        public static  int[,] decoupage_trame(string trame, int taille_carte)
        {
            int[,] carte = new int[taille_carte, taille_carte];

            string[] new_trame = new string[10];
            new_trame = trame.Split('|');

            for (int i = 0; i < taille_carte; i++)
            {
                string[] ligne_trame = new_trame[i].Split(':');
                for (int j = 0; j < taille_carte; j++)
                {
                    carte[i, j] = int.Parse(ligne_trame[j]);
                }
            }
            return carte;
        }

        public void traduction_ilot()
        {
            for (int ligne = 0; ligne < 10; ligne++)
            {
                for (int colonne = 0; colonne < 10; colonne++)
                {
                    liste_cases[ligne,colonne].decodeUneCase();
                }
            }
        }

        public  void afficher_ilot()
        {
            String chaine = "";
            int ligne, colonne, ligneparcolonne;
            for (ligne = 0; ligne < 10; ligne++)
            {
                for (ligneparcolonne = 1; ligneparcolonne < 3; ligneparcolonne++)
                {
                    if (ligneparcolonne == 1)
                    {
                        for (colonne = 0; colonne < 10; colonne++)
                        {
                            chaine = chaine + "+";
                            if (liste_cases[ligne, colonne].nord == true)
                            {
                                chaine += "---";
                            }
                            else
                            {
                                chaine += "   ";
                            }
                        }
                        chaine += "+";
                    }
                    if (ligneparcolonne == 2)
                    {
                        chaine += "| ";
                        for (colonne = 0; colonne < 10; colonne++)
                        {
                            chaine = liste_cases[ligne, colonne].Parcours_case(chaine);
                        }
                    }
                    Console.WriteLine(chaine);
                    chaine = "";
                }
            }
            Console.WriteLine("+---+---+---+---+---+---+---+---+---+---+ ");
        }

        //Procédure qui va servir à afficher correctement la carte elle permet de bien délimiter les parcelles (noms et délimitations comme la carte du serveur) 
        //et nous aide beaucoup pour l'algo de combat
        public void Valeurs_parcelles()
        {
            int nbascii = 97;
            char lettre_parcelle = (char)nbascii;
            int nbascii_save = nbascii;
            int ligne, colonne;
            for (ligne = 0; ligne < 10; ligne++)
            {
                for (colonne = 0; colonne < 10; colonne++)
                {
                    if (liste_cases[ligne, colonne].etat != "mer" && liste_cases[ligne, colonne].etat != "foret")
                    {
                        if (colonne != 0 && liste_cases[ligne, colonne].ouest == false && liste_cases[ligne, colonne - 1].remplie == true)
                        {
                            if (liste_cases[ligne, colonne - 1].etat != "mer" && liste_cases[ligne, colonne - 1].etat != "foret")
                                liste_cases[ligne, colonne].lettre_parcelle = liste_cases[ligne, colonne - 1].lettre_parcelle;
                        }
                        else
                        {
                            if (ligne != 0 && liste_cases[ligne, colonne].nord == false && liste_cases[ligne - 1, colonne].remplie == true)
                            {
                                if (liste_cases[ligne - 1, colonne].etat != "mer" && liste_cases[ligne - 1, colonne].etat != "foret")
                                    liste_cases[ligne, colonne].lettre_parcelle = liste_cases[ligne - 1, colonne].lettre_parcelle;
                            }
                            else
                            {
                                if (colonne != 9 && liste_cases[ligne, colonne].est == false && liste_cases[ligne, colonne + 1].remplie == true)
                                {
                                    if (liste_cases[ligne, colonne + 1].etat != "mer" && liste_cases[ligne, colonne + 1].etat != "foret")
                                        liste_cases[ligne, colonne].lettre_parcelle = liste_cases[ligne, colonne + 1].lettre_parcelle;
                                }
                                else
                                {
                                    if (ligne != 9 && liste_cases[ligne, colonne].sud == false && liste_cases[ligne + 1, colonne].remplie == true)
                                    {
                                        if (liste_cases[ligne + 1, colonne].etat != "mer" && liste_cases[ligne + 1, colonne].etat != "foret")
                                            liste_cases[ligne, colonne].lettre_parcelle = liste_cases[ligne + 1, colonne].lettre_parcelle;
                                    }
                                    else
                                    {
                                        liste_cases[ligne, colonne].lettre_parcelle = lettre_parcelle;
                                        nbascii += 1;
                                        lettre_parcelle = (char)nbascii;
                                        nb_parcelles += 1;
                                    }
                                }
                            }
                        }
                    }
                    if (liste_cases[ligne, colonne].sud == false && ligne != 9 && liste_cases[ligne + 1, colonne].remplie == false)
                    {
                        liste_cases[ligne + 1, colonne].lettre_parcelle = liste_cases[ligne, colonne].lettre_parcelle;
                        liste_cases[ligne + 1, colonne].remplie = true;
                    }
                    liste_cases[ligne, colonne].remplie = true;
                }
            }
            attribution_parcelles();
        }

        //Procédure qui définit toutes nos Parcelles avec leur nom et le nombre de cases qu'elles contiennent
        public void attribution_parcelles()
        {
            int nbascii = 97;
            char lettre_parcelle = (char)nbascii;
            for (int i = 0; i < nb_parcelles; i++)
            {
                Parcelles.Add(new Parcelle(lettre_parcelle));
                nbascii += 1;
                lettre_parcelle = (char)nbascii;
            }
            for (int ligne = 0; ligne < 10; ligne++)
            {
                for (int colonne = 0; colonne < 10; colonne++)
                {
                    if (liste_cases[ligne, colonne].etat != "mer" && liste_cases[ligne, colonne].etat != "foret")
                    {
                        Parcelles[(int)liste_cases[ligne, colonne].lettre_parcelle - 97].Cases.Add(liste_cases[ligne, colonne]);
                    }
                }
            }
        }

    }
}
