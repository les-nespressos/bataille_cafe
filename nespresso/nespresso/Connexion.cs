﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;

namespace nespresso
{
    class Connexion
    {
        public static Socket Create_Socket()
        {
            return new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        }

        public static void Connect_Socket(Socket mySocket)
        {
            try
            {
                mySocket.Connect("127.0.0.1", 1213);
                Console.Out.WriteLine("Connexion Reussie!");
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine("échec de connexion: {0}", ex.ToString());
            }
        }

        public static string Recup_Trame(Socket mySocket)
        {
            try
            {
                byte[] buffer = new byte[512];
                mySocket.Receive(buffer);
                string carte = Encoding.ASCII.GetString(buffer);
                Console.Out.WriteLine("voici la trame  : {0}", carte);
                return carte;
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine("recup impossible : {0}", ex.ToString());
                return null;
            }

        }


        public static void destroy_Socket(Socket mySocket)
        {
            try
            {
                mySocket.Shutdown(SocketShutdown.Both);
            }
            catch
            {
                mySocket.Close();
            }

        }

    }
}
