﻿using System;
using System.Collections.Generic;
using System.Text;

namespace nespresso
{
    public class Combat
    {
        public static string algo_combat(Carte carte, string coup_serveur)
        {
            //Découpage de de l'abscisse (x) et de l'ordonnée (y) du coup du serveur
            int x = int.Parse(coup_serveur.Substring(2, 1));
            int y = int.Parse(coup_serveur.Substring(3, 1));

            int ordonnee = 0, abcisse = 0;
            bool parcelle_choisie = false;

            //Déduction du nom de la parcelle jouée par le serveur en fonction des coordonnées du coup
            char nom_parcelle_adverse = carte.ilot.liste_cases[x, y].lettre_parcelle;


            //Stratégie de combat :
            //Chercher en prorité les parcelels à 5 cases, puis si aucune n'est compatible avec les règles ou libre
            //Chercher celles de 3 cases, puis 6 cases puis appliquer la strategie basique
            parcelle_choisie = strat_parcelle_cinq_cases(carte, nom_parcelle_adverse, ref ordonnee, ref abcisse, coup_serveur);

            if (parcelle_choisie == false)
                parcelle_choisie = strat_parcelle_trois_cases(carte, nom_parcelle_adverse, ref ordonnee, ref abcisse, coup_serveur);
            if (parcelle_choisie == false)
                parcelle_choisie = strat_parcelle_six_cases(carte, nom_parcelle_adverse, ref ordonnee, ref abcisse, coup_serveur);
            if (parcelle_choisie == false)
                parcelle_choisie = strategie_basique(carte, nom_parcelle_adverse, ref ordonnee, ref abcisse, coup_serveur);

            if (parcelle_choisie != false)
            {
                return ("A:" + abcisse + ordonnee);
            }

            else 
            {
                Random aleatoire_abcisse = new Random();
                Random aleatoire_ordonnee = new Random();
                aleatoire_abcisse.Next(0, 10);
                aleatoire_ordonnee.Next(0, 10);
                return ("A:" + aleatoire_abcisse + aleatoire_ordonnee); 
            }

        }

        //Au premier tour l'algo parcours les parcelles à la recherche d'une parcelle de 5 cases et joue dans une de ses cases
        public static string algo_premiertour(Carte carte)
        {
            int ordonnee = 0, abcisse = 0;
            foreach (Parcelle parcelle_traitee in carte.ilot.Parcelles)
            {
                if (parcelle_traitee.Cases.Count == 5)
                {
                    foreach (Case case_traitee in parcelle_traitee.Cases)
                    {
                        ordonnee = case_traitee.ordonnee;
                        abcisse = case_traitee.abcisse;
                    }
                }
            }

            return ("A:" + abcisse + ordonnee);
        }

        //Stratégie 3 cases : On recherche une parcelle de 3 cases où moins de 2 cases (1 ou moins par le joueur et 1 ou moins par le
        //serveur) sont jouées et où les règles de parcelles et de coordonnées sont respectées
        public static bool strat_parcelle_trois_cases(Carte carte, char nom_parcelle_adverse, ref int ordonnee, ref int abcisse, string coup_serveur)
        {
            int x = int.Parse(coup_serveur.Substring(2, 1));
            int y = int.Parse(coup_serveur.Substring(2, 2));
            bool parcelle_choisie = false;
            bool parcelle_valide = false;
            foreach (Parcelle parcelle_traitee in carte.ilot.Parcelles)
            {
                if (parcelle_traitee.Cases.Count == 3 && parcelle_traitee.nom != nom_parcelle_adverse && parcelle_choisie == false
                    && parcelle_traitee.Casesjoueesserveur + parcelle_traitee.Casesjoueesclient < 3)
                {
                    if (parcelle_traitee.Casesjoueesserveur == 0 && parcelle_traitee.Casesjoueesclient <= 1)
                    {
                        parcelle_valide = true;
                    }
                    else if (parcelle_traitee.Casesjoueesserveur == 1 && parcelle_traitee.Casesjoueesclient <= 1)
                    {
                            parcelle_valide = true;
                    }
                }
                if (parcelle_valide == true)
                {
                    foreach (Case case_traitee in parcelle_traitee.Cases)
                    {
                        if ((case_traitee.abcisse == x || case_traitee.ordonnee == y) && case_traitee.jouee == false)
                        {
                            parcelle_choisie = true;
                            ordonnee = case_traitee.ordonnee;
                            abcisse = case_traitee.abcisse;
                        }
                    }
                }
                parcelle_valide = false;

            }

            return parcelle_choisie;
        }

        //Stratégie 5 cases : On recherche une parcelle de 5 cases où moins de 3 cases (2 ou moins par le joueur et 2 ou moins par le
        //serveur) sont jouées et où les règles de parcelles et de coordonnées sont respectées
        public static bool strat_parcelle_cinq_cases(Carte carte, char nom_parcelle_adverse, ref int ordonnee, ref int abcisse, string coup_serveur)
        {
            int x = int.Parse(coup_serveur.Substring(2, 1));
            int y = int.Parse(coup_serveur.Substring(2, 2));
            bool parcelle_choisie = false;
            bool parcelle_valide = false;
            foreach (Parcelle parcelle_traitee in carte.ilot.Parcelles)
            {
                if (parcelle_traitee.Cases.Count == 5 && parcelle_traitee.nom != nom_parcelle_adverse && parcelle_choisie == false
                    && parcelle_traitee.Casesjoueesserveur + parcelle_traitee.Casesjoueesclient < 5)
                {
                    if (parcelle_traitee.Casesjoueesserveur == 0 && parcelle_traitee.Casesjoueesclient <= 2)
                    {
                        parcelle_valide = true;
                    }
                    else if (parcelle_traitee.Casesjoueesserveur == 1 && parcelle_traitee.Casesjoueesclient <= 2 && parcelle_traitee.Casesjoueesclient>=1)
                    {
                        parcelle_valide = true;
                    }
                    else if(parcelle_traitee.Casesjoueesserveur == 2 && parcelle_traitee.Casesjoueesclient == 2)
                    {
                        parcelle_valide = true;
                    }
                }
                if (parcelle_valide == true)
                {
                    foreach (Case case_traitee in parcelle_traitee.Cases)
                    {
                        if ((case_traitee.abcisse == x || case_traitee.ordonnee == y) && case_traitee.jouee == false)
                        {
                            parcelle_choisie = true;
                            ordonnee = case_traitee.ordonnee;
                            abcisse = case_traitee.abcisse;
                        }
                    }
                }
                parcelle_valide = false;

            }

            return parcelle_choisie;
        }

        //Stratégie 6 cases : On recherche une parcelle de 6 cases où moins de 4 cases (3 ou moins par le joueur et 2 ou moins par le
        //serveur) sont jouées et où les règles de parcelles et de coordonnées sont respectées
        public static bool strat_parcelle_six_cases(Carte carte, char nom_parcelle_adverse, ref int ordonnee, ref int abcisse, string coup_serveur)
        {
            int x = int.Parse(coup_serveur.Substring(2, 1));
            int y = int.Parse(coup_serveur.Substring(3, 1));
            bool parcelle_valide = false;
            bool parcelle_choisie = false;
            if (parcelle_choisie == false)
            {
                foreach (Parcelle parcelle_traitee in carte.ilot.Parcelles)
                {
                    if (parcelle_traitee.Cases.Count == 6 && parcelle_traitee.nom != nom_parcelle_adverse && parcelle_choisie == false
                        && parcelle_traitee.Casesjoueesserveur + parcelle_traitee.Casesjoueesclient < 6 && parcelle_traitee.Casesjoueesclient < 4 && parcelle_traitee.Casesjoueesserveur < 4)
                    {
                        if (parcelle_traitee.Casesjoueesserveur <= 2 && (parcelle_traitee.Casesjoueesclient == 3 || parcelle_traitee.Casesjoueesclient == 2))
                        {
                            parcelle_valide = true;
                        }
                        else if (parcelle_traitee.Casesjoueesserveur == 2 && parcelle_traitee.Casesjoueesclient == 2)
                        {
                            parcelle_valide = true;
                        }
                        else if (parcelle_traitee.Casesjoueesserveur == 3 && parcelle_traitee.Casesjoueesclient == 2)
                        {
                            parcelle_valide = true;
                        }
                        else if (parcelle_traitee.Casesjoueesclient >= parcelle_traitee.Casesjoueesserveur &&
                            parcelle_traitee.Casesjoueesserveur + parcelle_traitee.Casesjoueesclient <= 5)
                        {
                            parcelle_valide = true;
                        }
                        else if (parcelle_traitee.Casesjoueesserveur <= 1 && (parcelle_traitee.Casesjoueesclient == 2 || parcelle_traitee.Casesjoueesclient == 1))
                        {
                            parcelle_valide = true;
                        }
                        else if (parcelle_traitee.Casesjoueesserveur == 0 && parcelle_traitee.Casesjoueesclient <= 1)
                        {
                            parcelle_valide = true;
                        }
                    }
                    if (parcelle_valide == true)
                    {
                        foreach (Case case_traitee in parcelle_traitee.Cases)
                        {
                            if ((case_traitee.abcisse == x || case_traitee.ordonnee == y) && case_traitee.jouee == false)
                            {
                                parcelle_choisie = true;
                                ordonnee = case_traitee.ordonnee;
                                abcisse = case_traitee.abcisse;
                            }
                        }
                    }
                    parcelle_valide = false;
                }
            }
            return parcelle_choisie;
        }

        //Stratégie basique : On recherche une parcelle de plus de 2 cases où la moitié ou moins des cases sont jouées
        //et où les règles de parcelles et de coordonnées sont respectées
        public static bool strategie_basique(Carte carte, char nom_parcelle_adverse, ref int ordonnee, ref int abcisse, string coup_serveur)
        {
            int x = int.Parse(coup_serveur.Substring(2, 1));
            int y = int.Parse(coup_serveur.Substring(2, 2));
            bool parcelle_choisie = false;

            if (parcelle_choisie == false)
            {
                foreach (Parcelle parcelle_traitee in carte.ilot.Parcelles)
                {

                    if (parcelle_traitee.nom != nom_parcelle_adverse && parcelle_traitee.Cases.Count > 2 &&
                    parcelle_traitee.Casesjoueesserveur + parcelle_traitee.Casesjoueesclient <= parcelle_traitee.Cases.Count / 2 + 1 && parcelle_traitee.Casesjoueesserveur <= parcelle_traitee.Cases.Count / 2)
                    {
                        foreach (Case case_traitee in parcelle_traitee.Cases)
                        {
                            if ((case_traitee.abcisse == x || case_traitee.ordonnee == y) &&
                                case_traitee.jouee == false)
                            {
                                parcelle_choisie = true;
                                ordonnee = case_traitee.ordonnee;
                                abcisse = case_traitee.abcisse;
                            }
                        }
                    }
                }
            }
            if (parcelle_choisie == false)
            {
                for (int i = 0; i < 10; i++)
                {
                    for (int j = 0; j < 10; j++)
                    {
                        if ((carte.ilot.liste_cases[i, j].abcisse == x || carte.ilot.liste_cases[i, j].ordonnee == y) &&
                            carte.ilot.liste_cases[i, j].jouee == false && carte.ilot.liste_cases[i, j].lettre_parcelle != nom_parcelle_adverse)
                        {
                            parcelle_choisie = true;
                            abcisse = i;
                            ordonnee = j;
                        }
                    }
                }

            }
            return parcelle_choisie;
        }
    }
}
